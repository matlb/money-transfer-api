package repository;

import models.Transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository {

    Optional<Transaction> getTransaction(long id);

    List<Transaction> getTransactions();

    Transaction createTransaction(Transaction transaction);
}
