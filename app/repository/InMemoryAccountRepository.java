package repository;

import models.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryAccountRepository implements AccountRepository {

    private List<Account> accounts = new ArrayList<>();
    private long newUniqueIdentifier;

    @Override
    public Optional<Account> getAccount(long id) {
        return accounts.stream()
                .filter(account -> account.getId() == id)
                .findFirst();
    }

    @Override
    public List<Account> getAccounts() {
        return accounts;
    }

    @Override
    public Account createAccount(Account account) {
        Account newAccount = account.withId(newUniqueIdentifier++);
        accounts.add(newAccount);
        return newAccount;
    }

    @Override
    public Account updateAccount(Account account) {
        accounts = accounts.stream()
                .filter(a -> a.getId() != account.getId())
                .collect(Collectors.toList());

        accounts.add(account);
        return account;
    }
}
