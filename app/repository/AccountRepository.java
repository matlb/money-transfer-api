package repository;

import models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    Optional<Account> getAccount(long id);

    List<Account> getAccounts();

    Account createAccount(Account account);

    Account updateAccount(Account account);
}
