package repository;

import models.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryTransactionRepository implements TransactionRepository {

    private List<Transaction> transactions = new ArrayList<>();
    private long newUniqueIdentifier;

    @Override
    public Optional<Transaction> getTransaction(long id) {
        return transactions.stream()
                .filter(transaction -> transaction.getId() == id)
                .findFirst();
    }

    @Override
    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public Transaction createTransaction(Transaction transaction) {
        Transaction newTransaction = transaction.withId(newUniqueIdentifier++);
        transactions.add(newTransaction);
        return newTransaction;
    }
}
