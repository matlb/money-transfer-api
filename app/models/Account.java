package models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Account {
    private final long id;
    private final long balance;

    public Account(long id, long balance) {
        this.id = id;
        this.balance = balance;
    }

    @JsonCreator
    public Account(@JsonProperty("balance") long balance) {
        this.id = 0;
        this.balance = balance;
    }

    public Account withId(long id) {
        return new Account(id, balance);
    }

    public Account removeBalance(long balance) {
        return new Account(id, this.balance - balance);
    }

    public Account addBalance(long balance) {
        return new Account(id, this.balance + balance);
    }

    public long getId() {
        return id;
    }

    public long getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                balance == account.balance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }
}
