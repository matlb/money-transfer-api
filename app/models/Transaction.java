package models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Transaction {
    private final long id;
    private final long accountIdFrom;
    private final long accountIdTo;
    private final long amount;

    public Transaction(long id, long accountIdFrom, long accountIdTo, long amount) {
        this.id = id;
        this.accountIdFrom = accountIdFrom;
        this.accountIdTo = accountIdTo;
        this.amount = amount;
    }

    @JsonCreator
    public Transaction(@JsonProperty("accountIdFrom") long accountIdFrom,
                       @JsonProperty("accountIdTo") long accountIdTo, @JsonProperty("amount") long amount) {
        this.id = 0;
        this.accountIdFrom = accountIdFrom;
        this.accountIdTo = accountIdTo;
        this.amount = amount;
    }

    public Transaction withId(long id) {
        return new Transaction(id, accountIdFrom, accountIdTo, amount);
    }

    public long getId() {
        return id;
    }

    public long getAccountIdFrom() {
        return accountIdFrom;
    }

    public long getAccountIdTo() {
        return accountIdTo;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id == that.id &&
                accountIdFrom == that.accountIdFrom &&
                accountIdTo == that.accountIdTo &&
                amount == that.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountIdFrom, accountIdTo, amount);
    }
}
