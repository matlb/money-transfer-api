package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Account;
import models.Transaction;
import play.mvc.Controller;
import play.mvc.Result;
import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import repository.InMemoryTransactionRepository;
import repository.TransactionRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AccountController extends Controller {

    private AccountRepository accountRepository = new InMemoryAccountRepository();
    private TransactionRepository transactionRepository = new InMemoryTransactionRepository();
    private final ObjectMapper objectMapper = new ObjectMapper();

    public Result getAccounts() throws JsonProcessingException {
        List<Account> accounts = accountRepository.getAccounts();
        return ok(objectMapper.writeValueAsString(accounts));
    }

    public Result getAccount(long id) throws JsonProcessingException {
        Optional<Account> account = accountRepository.getAccount(id);

        if (account.isPresent()) {
            return ok(objectMapper.writeValueAsString(account.get()));
        } else {
            return notFound("Account doesn't exist");
        }
    }

    public Result createAccount() {
        try {
            Account account = objectMapper.readValue(request().body().asText(), Account.class);
            return ok(objectMapper.writeValueAsString(accountRepository.createAccount(account)));
        } catch (IOException e) {
            return badRequest("Malformed request");
        }
    }

    public Result getTransactions() throws JsonProcessingException {
        List<Transaction> transactions = transactionRepository.getTransactions();
        return ok(objectMapper.writeValueAsString(transactions));
    }

    public Result getTransaction(long id) throws JsonProcessingException {
        Optional<Transaction> transaction = transactionRepository.getTransaction(id);

        if (transaction.isPresent()) {
            return ok(objectMapper.writeValueAsString(transaction.get()));
        } else {
            return notFound("Transaction doesn't exist");
        }
    }

    public Result createTransaction() {
        try {
            Transaction transaction = objectMapper.readValue(request().body().asText(), Transaction.class);

            if (transaction.getAccountIdFrom() == transaction.getAccountIdTo()) {
                return badRequest("Cannot create transaction between the same account");
            }

            Optional<Account> accountFrom = accountRepository.getAccount(transaction.getAccountIdFrom());
            Optional<Account> accountTo = accountRepository.getAccount(transaction.getAccountIdTo());
            long amount = transaction.getAmount();

            if (accountFrom.isPresent() && accountTo.isPresent()) {
                accountRepository.updateAccount(accountFrom.get().removeBalance(amount));
                accountRepository.updateAccount(accountTo.get().addBalance(amount));

                return ok(objectMapper.writeValueAsString(transactionRepository.createTransaction(transaction)));
            }

            return notFound("Account(s) in transaction not found");
        } catch (IOException e) {
            return badRequest("Malformed request");
        }
    }
}
