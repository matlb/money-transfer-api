# money-transfer-api

#How to run
`sbt run`

#Endpoints
```
# Accounts
GET     /v1/accounts                   Returns all accounts
GET     /v1/accounts/:id               Returns account with the id :id
POST    /v1/accounts                   Creates an account e.g. {"balance": 20}

# Transactions
GET     /v1/transactions               Return all transactions
GET     /v1/transactions/:id           Returns transaction with the id :id
POST    /v1/transactions               Create a transaction e.g. {"accountIdFrom": 0,"accountIdTo": 1,"amount": 10}
```
