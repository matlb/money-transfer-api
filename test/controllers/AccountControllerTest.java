package controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Account;
import models.Transaction;
import org.junit.Test;
import play.Application;
import play.core.j.JavaResultExtractor;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class AccountControllerTest extends WithApplication {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void createAccount() throws IOException {
        Account expectedAccount = new Account(0, 20);

        Result result = postCreateAccountRequest(20);
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), Account.class), expectedAccount);
    }

    @Test
    public void createAccountWithBadRequestCausesBadRequest() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/v1/accounts")
                .bodyText("{\"asd\": 20}");

        Result result = route(app, request);
        assertEquals(BAD_REQUEST, result.status());
    }

    @Test
    public void getAccount() throws IOException {
        Account expectedAccount = new Account(0, 20);

        postCreateAccountRequest(20);
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/accounts/0");

        Result result = route(app, request);
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), Account.class), expectedAccount);
    }

    @Test
    public void getAccountThatDoesNotExistCausesNotFound() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/accounts/0");

        Result result = route(app, request);
        assertEquals(NOT_FOUND, result.status());
    }

    @Test
    public void getAccounts() throws IOException {
        List<Account> expectedAccounts = Arrays.asList(new Account(0, 20), new Account(1, 10));

        postCreateAccountRequest(20);
        postCreateAccountRequest(10);

        Result result = getAllAccounts();
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), new TypeReference<List<Account>>(){}), expectedAccounts);
    }

    @Test
    public void createTransaction() throws IOException {
        Transaction expectedTransaction = new Transaction(0, 0, 1, 10);

        postCreateAccountRequest(20);
        postCreateAccountRequest(10);
        Result result = postCreateTransactionRequest(0, 1, 10);
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), Transaction.class), expectedTransaction);

        List<Account> expectedAccountsAfterTransaction = Arrays.asList(new Account(0, 10), new Account(1, 20));
        result = getAllAccounts();
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), new TypeReference<List<Account>>(){}), expectedAccountsAfterTransaction);
    }

    @Test
    public void createTransactionBetweenSameAccountCausesBadRequest() throws IOException {
        postCreateAccountRequest(20);
        Result result = postCreateTransactionRequest(0, 0, 10);
        assertEquals(BAD_REQUEST, result.status());
    }

    @Test
    public void createTransactionOnAccountThatDoesNotExistCausesNotFound() throws IOException {
        postCreateAccountRequest(20);
        Result result = postCreateTransactionRequest(0, 1, 10);
        assertEquals(NOT_FOUND, result.status());
    }

    @Test
    public void createTransactionWithBadRequestCausesBadRequest() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/v1/transactions")
                .bodyText("{\"asd\": 20}");

        Result result = route(app, request);
        assertEquals(BAD_REQUEST, result.status());
    }

    @Test
    public void getTransaction() throws IOException {
        Transaction expectedTransaction = new Transaction(0, 0, 1, 10);

        postCreateAccountRequest(20);
        postCreateAccountRequest(10);
        postCreateTransactionRequest(0, 1, 10);
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/transactions/0");

        Result result = route(app, request);
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), Transaction.class), expectedTransaction);
    }

    @Test
    public void getTransactionThatDoesNotExistCausesNotFound() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/transaction/0");

        Result result = route(app, request);
        assertEquals(NOT_FOUND, result.status());
    }

    @Test
    public void getTransactions() throws IOException {
        List<Transaction> expectedTransactions = Arrays.asList(new Transaction(0, 0, 1, 10), new Transaction(1, 1, 0, 20));

        postCreateAccountRequest(20);
        postCreateAccountRequest(10);
        postCreateTransactionRequest(0, 1, 10);
        postCreateTransactionRequest(1, 0, 20);
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/transactions");

        Result result = route(app, request);
        assertEquals(OK, result.status());
        assertEquals(objectMapper.readValue(Helpers.contentAsString(result), new TypeReference<List<Transaction>>(){}), expectedTransactions);
    }

    private Result postCreateAccountRequest(long balance) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/v1/accounts")
                .bodyText("{\"balance\": " + balance + "}");

        return route(app, request);
    }

    private Result getAllAccounts() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/v1/accounts");
        return route(app, request);
    }

    private Result postCreateTransactionRequest(long from, long to, long amount) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/v1/transactions")
                .bodyText("{\"accountIdFrom\": " + from + ",\"accountIdTo\": " + to + ",\"amount\": " + amount + "}");

        return route(app, request);
    }
}
